import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { TokenStorageService } from '../services/token-storage.service'
import { ToastrService } from 'ngx-toastr'

@Injectable()

export class AuthGuardService implements CanActivate {

  constructor (private auth: TokenStorageService,
    private router: Router,
    private toastr: ToastrService) {
  }

  canActivate (
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    console.log('AuthGuard#canActivate called')

    let url: string = state.url
    console.log(url)

    return this.checkLogin(url)
  }

  checkLogin (url: string): boolean {
    if (url === '/auth/password') {
      if (!this.auth.getUsername()) {
        this.router.navigate(['auth/login'])
        this.toastr.error('Debes colocar tu correo primero', 'Error', {
          progressBar: true,
          closeButton: true
        })
        return false
      }
      return true
    }
    if (!this.auth.getPassword()) {
      this.router.navigate(['auth/password'])

      return false
    }
    return true
  }
}
