import { Injectable } from '@angular/core'
import { Router, CanActivate } from '@angular/router'
import { TokenStorageService } from '../services/token-storage.service'

@Injectable({
  providedIn: 'root'
})

export class DashboardGuardService implements CanActivate {

  constructor (private auth: TokenStorageService, private router: Router) {
    console.log('hola')
  }

  canActivate (): boolean {
    if (this.auth.getToken()) {
      this.router.navigate(['/dashboard'])
      return false
    }
    return true
  }
}
