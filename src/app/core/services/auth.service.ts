import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'

import { JwtResponse } from '../../shared/models/jwt-response'
import { AuthLoginInfo } from '../../shared/models/login-info'
import { SingUpInfo } from '../../shared/models/signup-info'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor (private http: HttpClient) {
  }

  private loginUser = 'http://127.0.0.1:3000/api/auth/signinUser'
  private loginPass = 'http://127.0.0.1:3000/api/auth/signinPass'
  private signupUrl = 'http://127.0.0.1:3000/api/auth/signup'

  attempAuthUser (credentials: AuthLoginInfo): Observable<string> {
    return this.http.post<string>(this.loginUser, credentials)
  }
  attempAuthPass (credentials: AuthLoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginPass, credentials, httpOptions)
  }
  signUp (info: SingUpInfo): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions)
  }
}
