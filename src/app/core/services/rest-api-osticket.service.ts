import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable, Subject } from 'rxjs'
import 'rxjs/Rx'
import 'rxjs/add/operator/toPromise'

@Injectable({
  providedIn: 'root'
})
export class RestApiOsticketService {
  apiURL = 'http://127.0.0.1:3000'
  username: string
  sUsername = new Subject<string>()
  nOpen = new Subject<number>()
  numOpen: number

  constructor (private http: HttpClient) {}

  setUsername (username: string) {
    this.username = username
    this.sUsername.next(this.username)
  }

  getUsername (): Observable<string> {
    return this.sUsername.asObservable()
  }

  setNumOpen (numOpen: number) {
    this.numOpen = numOpen
    this.nOpen.next(this.numOpen)
  }

  getNumOpen (): Observable<number> {
    return this.nOpen.asObservable()
  }

  // HttpClient API post() method => Create ticket
  createTickets (tickets: any): Observable<{}> {
    try {
      return this.http.post(this.apiURL + '/api/create-ticket', tickets)
    } catch (error) {
      console.log(error)
    }
  }

  async getInfoTickets (numticket: string): Promise<any> {
    try {
      return await this.http.post(this.apiURL + '/api/info-ticket', { numticket: numticket }).toPromise()
    } catch (error) {
      console.log(error)
    }
  }

  async replyTicket (dataReply: any): Promise<any> {
    try {
      return await this.http.post(this.apiURL + '/api/reply-ticket', dataReply).toPromise()
    } catch (error) {
      console.log(error)
    }
  }

  getListTickets (username: string, status: string): Observable<any> {
    try {
      return this.http.post(this.apiURL + '/api/staff-tickets', { username: username, status: status })
    } catch (error) {
      console.log(error)
    }
  }
}
