import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ErrorPageComponent } from './shared/components/404/error-page/error-page.component'
import { AppComponent } from './app.component'
import { IndexComponent } from './modules/index/index.component'

const appRoutes: Routes = [
  {
    path: 'home',
    component: IndexComponent
  },
  { path: 'auth',
    loadChildren: './modules/auth/auth.module#AuthModule'
  },
  { path: 'dashboard',
    loadChildren: './modules/home/home.module#homeModule'
  },
  { path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  { path: '404',
    component: ErrorPageComponent
  },
  { path: '**',
    redirectTo: '404'
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
