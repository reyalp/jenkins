import { Component, OnInit } from '@angular/core'
import { TokenStorageService } from '../../../core/services/token-storage.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})

export class NavBarComponent implements OnInit {
  roles: string[]
  authority: string
  navbar = false
  title = 'White Hat Intranet'
  email = ''

  constructor (private tokenStorage: TokenStorageService) {
    console.log('hola')
  }

  ngOnInit () {
    if (this.tokenStorage.getToken()) {
      this.email = this.tokenStorage.getUsername()
      this.navbar = true
      this.roles = this.tokenStorage.getAuthorities()
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin'
          return true
        } else if (role === 'ROLE_PM') {
          this.authority = 'pm'
          return true
        }
        this.authority = 'user'
        return true
      })
    }
  }

  salir () {
    this.tokenStorage.signOut()
  }
}
