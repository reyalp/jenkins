export class SingUpInfo {
  email: string
  roles: string[]
  password: string

  constructor (email: string, password: string) {
    this.email = email
    this.password = password
    this.roles = ['user']
  }
}
