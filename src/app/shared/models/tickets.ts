export class Ticket {
    number: string;
    last_Update: string;
    subject: string;
    from: string;
    priority: string;
    assigned_to: string;
}
