export class Ticket {
    numticket: number;
    subject: string;
    status: string;
    priority: string;
    department: string;
    create_timestamp: string;
    user_name: string;
    user_email: string;
    user_phone: string;
    source: string;
    due_timestamp: string;
    help_topic: string;
    assigned_to: string;
    message: string;
}
