import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { ToastrModule } from 'ngx-toastr'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MaterialModule } from './material.module'

import { AppRoutingModule } from './app-routing.module'

import { AppComponent } from './app.component'
import { httpInterceptorProviders } from './core/interceptors/auth-interceptor'
import { DashboardGuardService } from './core/guards/Dashboard-guard'
import { ErrorPageComponent } from './shared/components/404/error-page/error-page.component'
import { FooterComponent } from './shared/components/footer/footer.component'
import { IndexComponent } from './modules/index/index.component'
import { NavComponent } from './modules/index/nav/nav.component'
import { FarmanetComponent } from './modules/index/farmanet/farmanet.component'
import { ServiciosComponent } from './modules/index/servicios/servicios.component'
import { QuienesSomosComponent } from './modules/index/quienes-somos/quienes-somos.component'
import { ContactanosComponent } from './modules/index/contactanos/contactanos.component'

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    IndexComponent,
    NavComponent,
    FarmanetComponent,
    ServiciosComponent,
    QuienesSomosComponent,
    ContactanosComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-bottom-left',
      preventDuplicates: true
    }) // ToastrModule added
  ],
  providers: [ httpInterceptorProviders, DashboardGuardService ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {}
