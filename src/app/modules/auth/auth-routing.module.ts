import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from './components/login/login.component'
import { LoginPassComponent } from './components/login-password/login-password.component'
import { AuthGuardService as AuthGuard } from '../../core/guards/auth-guard'
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component'
import { RegisterComponent } from './components/register/register.component'

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { animation: 'LoginPage' }
  },
  { path: 'password',
    component: LoginPassComponent,
    data: { animation: 'PasswordPage' },
    canActivate: [ AuthGuard ]
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'register-data',
    component: RegisterComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthRoutingModule {
  static components = [
    LoginComponent,
    LoginPassComponent,
    ForgotPasswordComponent
  ]
}
