import { async, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: []
    }) .compileComponents();
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ]
    }).createComponent(LoginComponent);

  }));

  afterEach(async(()=>{

  }));

  it('Email:', async(() => {
    (<HTMLInputElement>document.getElementById('email')).value='soporte@white-hat.com.ve';
    document.getElementById('siguiente').click();
    expect((<HTMLInputElement>document.getElementById('email')).value).toBe('soporte@white-hat.com.ve');

  }));
});

