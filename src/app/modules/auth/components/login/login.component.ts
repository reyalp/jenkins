import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { TokenStorageService } from '../../../../core/services/token-storage.service'
import { AuthService } from '../../../../core/services/auth.service'
import { AuthLoginInfo } from '../../../../shared/models/login-info'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  form: any = {}
  isLoggedIn = false
  isLoginFailed = false
  isDisabled = false
  errorMessage = ''
  loaded = false
  roles: string [] = []
  loginInfo: any = {}

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ])

  matcher = new MyErrorStateMatcher()

  constructor (private tokenStorage: TokenStorageService, private authService: AuthService, private router: Router, private toastr: ToastrService) {

  }

  ngOnInit () {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true
      this.roles = this.tokenStorage.getAuthorities()
    }
  }

  toasterClickedHandler () {
    console.log('Toastr clicked')
  }

  Error () {
    if (!this.emailFormControl.errors) {
      this.isDisabled = false
    } else {
      this.loaded = false
      this.isDisabled = true
    }
  }

  onSubmit () {
    this.loaded = true
    this.loginInfo = new AuthLoginInfo(
      this.form.username,
      this.form.password
    )

    setTimeout(() => {
      if (!this.emailFormControl.errors) {
        this.authService.attempAuthUser(this.loginInfo).subscribe(
          (data: any) => {
            this.tokenStorage.saveUsername(data.username)
            this.isLoginFailed = false
            if (this.tokenStorage.getUsername()) {
              this.router.navigate(['auth/password'])
            }
          },
          error => {
            console.log(error)
            this.loaded = false
            if (error.status === 0) {
              this.toastr.error('Servidor Apagado', 'Error de Red', {
                progressBar: true,
                closeButton: true
              })
            } else {
              this.isLoginFailed = true
              this.toastr.error(error.error.reason, 'Error', {
                progressBar: true,
                closeButton: true
              })
            }
          }
        )
      } else {
        this.isDisabled = true
        this.loaded = false
      }
    }, 1000)
  }
}
