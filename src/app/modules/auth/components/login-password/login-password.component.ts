
import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { TokenStorageService } from '../../../../core/services/token-storage.service'
import { AuthService } from '../../../../core/services/auth.service'
import { AuthLoginInfo } from '../../../../shared/models/login-info'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}

@Component({
  selector: 'app-login-pass',
  templateUrl: './login-password.component.html',
  styleUrls: ['./login-password.component.css']
})

export class LoginPassComponent implements OnInit {
  form: any = {}
  isLoggedIn = false
  isLoginFailed = false
  errorMessage = ''
  isDisabled = false
  loaded = false
  roles: string [] = []
  loginInfo: AuthLoginInfo
  email = ''

  passFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(8)
  ])

  matcher = new MyErrorStateMatcher()

  constructor (private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    private toastr: ToastrService) {
  }

  ngOnInit () {
    this.email = this.tokenStorage.getUsername()
  }

  onSubmit () {
    this.loaded = true

    this.loginInfo = new AuthLoginInfo(
      this.tokenStorage.getUsername(),
      this.form.password
    )

    if (!this.passFormControl.errors) {
      this.authService.attempAuthPass(this.loginInfo).subscribe(
        (data: any) => {
          this.tokenStorage.saveToken(data.accessToken)
          this.tokenStorage.saveAuthorities(data.authorities)
          this.isLoginFailed = false
          this.isLoggedIn = true
          this.router.navigate(['home/dashboard'])
          // window.location.assign("http://localhost:4200/home/dashboard")
        },
        error => {
          console.log(error)
          this.loaded = false
          if (error.status === 0) {
            this.toastr.error(error.message, 'Error de Red', {
              progressBar: true,
              closeButton: true
            })
          } else {
            this.isLoginFailed = true
            this.toastr.error(error.error.reason, 'Error', {
              progressBar: true,
              closeButton: true
            })
          }
        }
      )
    } else {
      this.isDisabled = true
      this.loaded = false
    }
  }
  reloadPage () {
    window.location.reload()
  }
}
