import { async, TestBed } from '@angular/core/testing';

import { LoginPassComponent } from './login-password.component';

describe('LoginPassComponent', () => {
 
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPassComponent ],
      providers: []
    }) .compileComponents();
    TestBed.configureTestingModule({
      declarations: [ LoginPassComponent ]
    }).createComponent(LoginPassComponent);

  }));

  afterEach(async(()=>{

  }));

  it('Contraseña:', async(() => {
    (<HTMLInputElement>document.getElementById('password')).value='123';
    document.getElementById('entrar').click();
    expect((<HTMLInputElement>document.getElementById('password')).value).toBe('123')

  }));
});
