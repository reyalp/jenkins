import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { TokenStorageService } from '../../../../core/services/token-storage.service'
import { AuthService } from '../../../../core/services/auth.service'
import { AuthLoginInfo } from '../../../../shared/models/login-info'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: any = {}
  isLoggedIn = false
  isLoginFailed = false
  isDisabled = false
  errorMessage = ''
  loaded = false
  roles: string [] = []
  loginInfo: any = {}

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ])

  matcher = new MyErrorStateMatcher()

  constructor () {
    console.log()
  }

  ngOnInit () {
  }

}
