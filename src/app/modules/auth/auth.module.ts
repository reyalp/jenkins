import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { MaterialModule } from '../../material.module'

import { AuthRoutingModule } from './auth-routing.module'
import { AuthGuardService as AuthGuard } from '../../core/guards/auth-guard'
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component'
import { RegisterComponent } from './components/register/register.component'

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    MaterialModule, // Material module added
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ AuthRoutingModule.components, ForgotPasswordComponent, RegisterComponent ],
  providers: [ AuthGuard ]
})

export class AuthModule { }
