import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { CommonModule } from '@angular/common'

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModule } from '../../material.module'
import { HomeComponent } from './home.component'
import { DashboardComponent } from './components/dashboard.component'
import { RegisterComponent } from './components/register/register.component'
import { MenuRoutingModule } from './home-routing.module'
import { NavBarComponent } from '../../shared/components/menu/nav-bar.component'
import { PerfilComponent } from './components/perfil/perfil.component'
import { TicketsViewComponent } from './components/ostickets/tickets-view/tickets-view.component'
import { TicketsListComponent } from './components/ostickets/tickets-list/tickets-list.component'
import { TicketsCreateComponent } from './components/ostickets/tickets-create/tickets-create.component'
import { TicketsDetailsComponent } from './components/ostickets/tickets-details/tickets-details.component'
import { TicketsViewMenuComponent } from './components/ostickets/tickets-view-menu/tickets-view-menu.component'

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    RegisterComponent,
    NavBarComponent,
    PerfilComponent,
    TicketsViewComponent,
    TicketsListComponent,
    TicketsCreateComponent,
    TicketsDetailsComponent,
    TicketsViewMenuComponent,
    PerfilComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    MaterialModule, // Material module added
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})

export class HomeModule {}
