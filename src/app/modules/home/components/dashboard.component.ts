import { Component, OnInit } from '@angular/core'
import { TokenStorageService } from '../../../core/services/token-storage.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  email = ''
  cargo = true
  isLoggedIn = false
  roles: string [] = []

  constructor (private tokenStorage: TokenStorageService,
    private ruta: Router) {
  }

  ngOnInit () {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true
      this.email = this.tokenStorage.getUsername()
      this.roles = this.tokenStorage.getAuthorities()
    }
  }
}
