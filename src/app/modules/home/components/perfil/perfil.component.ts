import { Component, OnInit } from '@angular/core'
import { TokenStorageService } from '../../../../core/services/token-storage.service'

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent implements OnInit {
  roles: string[]
  authority: string

  constructor (private token: TokenStorageService) {
  }

  ngOnInit () {
    this.roles = this.token.getAuthorities()
    this.roles.every(role => {
      if (role === 'ROLE_ADMIN') {
        this.authority = 'admin'
        return true
      } else if (role === 'ROLE_PM') {
        this.authority = 'pm'
        return true
      }
      this.authority = 'user'
      return true
    })
  }
}
