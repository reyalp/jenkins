import { Component, OnInit } from '@angular/core'
import { SingUpInfo } from '../../../../shared/models/signup-info'
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { AuthService } from '../../../../core/services/auth.service'
import { Router } from '@angular/router'

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {}
  isSignedUp = false
  isSignUpFailed = false
  errorMessage = ''
  loaded = false
  isDisabled = false

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ])

  passFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(8)
  ])

  matcher = new MyErrorStateMatcher()

  constructor (private authService: AuthService,
    private router: Router,
    private signupInfo: SingUpInfo) {
  }

  ngOnInit () {
  }

  ErrorE () {
    if (!this.emailFormControl.errors) {
      this.isDisabled = false
    } else this.isDisabled = true
  }

  ErrorP () {
    if (!this.passFormControl.errors) {
      this.isDisabled = false
    } else this.isDisabled = true
  }

  onSubmit () {
    this.loaded = true
    this.signupInfo = new SingUpInfo(
      this.form.email,
      this.form.password)

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        console.log(data)
        this.isSignedUp = true
        this.isSignUpFailed = false
        setTimeout(() => {
          this.router.navigate(['auth/login'])
        }, 3000)
      },
      error => {
        console.log(error)

        this.loaded = false
        this.isSignUpFailed = true
        if (error.status === 0) {
          this.errorMessage = 'Server Off!!'
        } else {
          this.errorMessage = error.error
        }
      }
    )
  }
}
