import { Component, OnInit } from '@angular/core'
import { RestApiOsticketService } from '../../../../../core/services/rest-api-osticket.service'

@Component({
  selector: 'tickets-view-menu',
  templateUrl: './tickets-view-menu.component.html',
  styleUrls: ['./tickets-view-menu.component.css']
})
export class TicketsViewMenuComponent implements OnInit {
  numOpen: number

  constructor (private restApiOstickets: RestApiOsticketService) { }

  ngOnInit () {
    this.restApiOstickets.getNumOpen().subscribe((num: number) => {
      this.numOpen = num
    })
  }
}
