import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsViewMenuComponent } from './tickets-view-menu.component';

describe('ViewTicketsMenuComponent', () => {
  let component: TicketsViewMenuComponent;
  let fixture: ComponentFixture<TicketsViewMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsViewMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsViewMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
