import { Component, OnInit, Input } from '@angular/core'
import { RestApiOsticketService } from '../../../../../core/services/rest-api-osticket.service'
import { Router } from '@angular/router'
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}

@Component({
  selector: 'app-tickets-create',
  templateUrl: './tickets-create.component.html',
  styleUrls: ['./tickets-create.component.css']
})

export class TicketsCreateComponent implements OnInit {
  @Input() ticketsDetails = { name: '', email: '', phone: '', subject: '', message: '', assignee: '' }
  emailFormControl = new FormControl('', [Validators.required, Validators.email])
  matcher = new MyErrorStateMatcher()

  constructor (private restApiOstickets: RestApiOsticketService, private router: Router) {}

  ngOnInit () {}

  addTickets () {
    this.restApiOstickets.createTickets(this.ticketsDetails).subscribe(data => {
      console.log('Respuesta: ' + data)
      this.router.navigate(['/ostickets/tickets/Open'])
    })
  }
}
