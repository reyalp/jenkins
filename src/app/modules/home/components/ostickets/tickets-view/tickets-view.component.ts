import { Component, OnInit } from '@angular/core'
import { RestApiOsticketService } from '../../../../../core/services/rest-api-osticket.service'

@Component({
  selector: 'tickets-view',
  templateUrl: './tickets-view.component.html',
  styleUrls: ['./tickets-view.component.css']
})
export class TicketsViewComponent implements OnInit {

  constructor (private restApiOstickets: RestApiOsticketService) {}

  ngOnInit () {
    this.restApiOstickets.setUsername('anlli')
  }
}
