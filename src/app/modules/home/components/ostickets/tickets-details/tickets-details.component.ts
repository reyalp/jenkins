import { Component, OnInit, Input } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { RestApiOsticketService } from '../../../../../core/services/rest-api-osticket.service'

@Component({
  selector: 'app-tickets-details',
  templateUrl: './tickets-details.component.html',
  styleUrls: ['./tickets-details.component.css']
})

export class TicketsDetailsComponent implements OnInit {

  numticket = this.actRoute.snapshot.params['numticket']
  data: any = {}
  ticketsData: any = {}
  threadEntries: any[] = []
  showDetails: boolean
  @Input() ticketsReply = { ticketNumber: '', emailreply: '', response: '', reply_status_id: '', staffUserName: 'anlli' }

  constructor (private actRoute: ActivatedRoute, private restApiOstickets: RestApiOsticketService, private router: Router) {}

  async ngOnInit () {
    await this.restApiOstickets.getInfoTickets(this.numticket).then((resp: any) => {
      this.data = JSON.parse(resp)
      this.ticketsData = this.data.ticket
      this.threadEntries = this.ticketsData.thread_entries
      this.ticketsReply.ticketNumber = this.ticketsData.ticket_number
      this.ticketsReply.emailreply = this.ticketsData.user_email.address
      this.showDetails = true
    })
  }

  showReply (type: String, estatus: String) {
    if (type === 'R' && estatus === 'Closed') {
      return true
    }
    return false
  }

  async postReply () {
    await this.restApiOstickets.replyTicket(this.ticketsReply).then((resp: any) => {
      this.data = JSON.parse(resp)
      console.log('respuesta: ' + JSON.stringify(this.data.status_msg))
      this.router.navigate(['/home/ostickets/tickets/Open'])
    })
  }
}
