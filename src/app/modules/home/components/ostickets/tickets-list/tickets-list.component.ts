import { Component, OnInit, ViewChild } from '@angular/core'
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { RestApiOsticketService } from '../../../../../core/services/rest-api-osticket.service'
import { Ticket } from '../../../../../shared/models/ticket'

@Component({
  selector: 'tickets-list',
  templateUrl: './tickets-list.component.html',
  styleUrls: ['./tickets-list.component.css']
})

export class TicketsListComponent implements OnInit {

  displayedColumns: string[] = ['Number', 'Last Update', 'Subject', 'From', 'Priority', 'Assigned to']
  status: string
  tickets: Ticket[] = []
  username: string
  dataSource: MatTableDataSource<Ticket>
  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  constructor (private actRoute: ActivatedRoute, private restApiOstickets: RestApiOsticketService) {}

  ngOnInit () {
    this.actRoute.paramMap.subscribe((params: ParamMap) => {
      this.status = params.get('status')
      this.getList()
    })
  }

  getList () {
    this.restApiOstickets.getListTickets('anlli', this.status).subscribe((data: any) => {
      this.tickets = data.tickets
      this.dataSource = new MatTableDataSource(this.tickets)
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
      if (this.status === 'Open') {
        this.restApiOstickets.setNumOpen(this.tickets.length)
      }
    })
  }

  applyFilter (filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
}
