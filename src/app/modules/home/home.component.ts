import { Component, OnInit } from '@angular/core'
import { TokenStorageService } from '../../core/services/token-storage.service'
import { Router } from '@angular/router'

@Component({
  selector: 'home-dashboard',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  email = ''
  cargo = true
  isLoggedIn = false
  roles: string [] = []

  constructor (private tokenStorage: TokenStorageService,
    private ruta: Router) {

  }

  ngOnInit () {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true
      this.email = this.tokenStorage.getUsername()
      this.roles = this.tokenStorage.getAuthorities()
    }
  }
}
