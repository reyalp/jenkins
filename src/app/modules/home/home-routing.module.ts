import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './home.component'
import { DashboardComponent } from './components/dashboard.component'
import { RegisterComponent } from './components/register/register.component'
import { TicketsViewComponent } from './components/ostickets/tickets-view/tickets-view.component'
import { PerfilComponent } from './components/perfil/perfil.component'
import { TicketsListComponent } from './components/ostickets/tickets-list/tickets-list.component'
import { TicketsCreateComponent } from './components/ostickets/tickets-create/tickets-create.component'
import { TicketsDetailsComponent } from './components/ostickets/tickets-details/tickets-details.component'

const appRoutes: Routes = [
  { path: '',
    component: HomeComponent,
    children: [
      { path: 'dashboard',
        component: DashboardComponent },
      { path: 'ostickets',
        component: TicketsViewComponent,
        children: [
          { path: '', redirectTo: 'open', pathMatch: 'full' },
          { path: 'tickets/:status', component: TicketsListComponent },
          { path: 'new-ticket', component: TicketsCreateComponent },
          { path: 'ticket/:numticket', component: TicketsDetailsComponent }
        ]
      },
      { path: 'register',
        component: RegisterComponent },
      { path: 'perfil',
        component: PerfilComponent }
    ] }
]

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class MenuRoutingModule {
  static components = [
    RegisterComponent
  ]
}
