import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmanetComponent } from './farmanet.component';

describe('FarmanetComponent', () => {
  let component: FarmanetComponent;
  let fixture: ComponentFixture<FarmanetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmanetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmanetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
