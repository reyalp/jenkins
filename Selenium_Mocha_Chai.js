const { Builder, By, until } = require('selenium-webdriver');
const { expect } = require('chai');
const fs = require('fs');

describe('Intranet White-Hat', () => {
    //let driver;
    let driver1;
    it('Abrir Navegador ', async() => {
        //driver = await new Builder().forBrowser('chrome').build();
        driver1 = await new Builder().forBrowser('firefox').build();


    });

    it('Abrir  pagina', async() => {
        //await driver.get("http://localhost:8080/home");
        await driver1.get("http://localhost:8080/home");
        //  await driver2.get("http://localhost:8080/home");
        //await driver.Navigate().GoToUrl("http://localhost:8080/home");


    });
    it('Resolucion: 1366x768', async() => {

        //await driver.manage().window().setRect({ width: 1366, height: 768 });
        await driver1.manage().window().setRect({ width: 1366, height: 768 });
        //let ima = await driver.takeScreenshot();
        //await fs.writeFileSync('Captures/chrome1366x768.png', ima, 'base64');
        let imagen = await driver1.takeScreenshot();
        await fs.writeFileSync('Captures/firefox1366x768.png', imagen, 'base64')

    });/*
    it('Resolucion: 1024x768', async() => {
        await driver.manage().window().setRect({ width: 1024, height: 768 });
        //await driver1.manage().window().setRect({ width: 1024, height: 768 });
        let image = await driver.takeScreenshot();
        await fs.writeFileSync('Captures/chrome1024x768.png', image, 'base64')
        //let imagen = await driver1.takeScreenshot();
        //await fs.writeFileSync('Captures/firefox1024x768.png', imagen, 'base64')
    });
    it('Resolucion: 1280x1024', async() => {
        await driver.manage().window().setRect({ width: 1280, height: 1024 });
        //await driver1.manage().window().setRect({ width: 1280, height: 1024 });
        let image = await driver.takeScreenshot();
        await fs.writeFileSync('Captures/chrome1280x1024.png', image, 'base64')
        //let imagen = await driver1.takeScreenshot();
        //await fs.writeFileSync('Captures/firefox1280x1024.png', imagen, 'base64')
    });
    it('Resolucion:1280x800', async() => {
        await driver.manage().window().setRect({ width: 1280, height: 800 });
        //await driver1.manage().window().setRect({ width: 1280, height: 800 });
        let image = await driver.takeScreenshot();
        await fs.writeFileSync('Captures/chrome1280x800.png', image, 'base64')
        //let imagen = await driver1.takeScreenshot();
        //await fs.writeFileSync('Captures/firefox1280x800.png', imagen, 'base64')
    });
    it('Resolucion: 1920x1080', async() => {
        await driver.manage().window().setRect({ width: 1920, height: 1080 });
        //await driver1.manage().window().setRect({ width: 1920, height: 1080 });
        let image = await driver.takeScreenshot();
        await fs.writeFileSync('Captures/chrome1920x1080.png', image, 'base64')
        //let imagen = await driver1.takeScreenshot();
        //await fs.writeFileSync('Captures/firefox1920x1080.png', imagen, 'base64')
    });
    it('Resolucion: 800x600', async() => {
        await driver.manage().window().setRect({ width: 800, height: 600 });
        //await driver1.manage().window().setRect({ width: 800, height: 600 });
        let image = await driver.takeScreenshot();
        await fs.writeFileSync('Captures/chrome800x600.png', image, 'base64')
        //let imagen = await driver1.takeScreenshot();
        //await fs.writeFileSync('Captures/firefox800x600.png', imagen, 'base64')
    });
    /* it('Verificar Titulo', async() => {
         await driver.wait(until.elementLocated(By.id('search')));
         const title = await driver.getTitle();
         expect(title).to.equal('IntranetWhiteHat');
     });*//*
    it('Hacer un Registro', async() => {
        await driver.findElement({ xpath: '//*[@id="inicio"]/div/div/div[2]/div/div[3]/div[1]/a/span' }).click()
        //await driver1.findElement({ xpath: '//*[@id="inicio"]/div/div/div[2]/div/div[3]/div[1]/a/span' }).click()
        await driver.findElement(By.name('email')).sendKeys('yanitzabel@hotmail.es')
        //await driver1.findElement(By.name('email')).sendKeys('yanitzabel@hotmail.es')
        await driver.findElement({ xpath: '//*[@id="mat-input-1"]' }).sendKeys('04245370818')
        //await driver1.findElement({ xpath: '//*[@id="mat-input-1"]' }).sendKeys('04245370818')
        await driver.findElement({ xpath: '/html/body/app-root/div/app-register/div/div/div[1]/div/form/div[2]/div[2]/button' }).click()
        //await driver1.findElement({ xpath: '/html/body/app-root/div/app-register/div/div/div[1]/div/form/div[2]/div[2]/button' }).click()
        await driver.findElement(By.linkText('Crear Cuenta')).click()
        //await driver1.findElement(By.linkText('Crear Cuenta')).click()
    });
    it('Iniciar Sesion', async() => {
        await driver.findElement({ xpath: '//*[@id="inicio"]/div/div/div[2]/div/div[3]/div[2]/a/span' }).click()
        //await driver1.findElement({ xpath: '//*[@id="inicio"]/div/div/div[2]/div/div[3]/div[2]/a/span' }).click()
        await driver.findElement(By.name('email')).sendKeys('yanitzabel@hotmail.es')
        //await driver1.findElement(By.name('email')).sendKeys('yanitzabel@hotmail.es')
        await driver.findElement({ xpath: '/html/body/app-root/div/app-login/div/div/div[1]/div/form/div[2]/div[2]/button/span' }).click()
        //await driver1.findElement({ xpath: '/html/body/app-root/div/app-login/div/div/div[1]/div/form/div[2]/div[2]/button/span' }).click()
        //await driver1.findElement(By.linkText('¿Has olvidado tu correo electrónico?')).click()
        await driver.findElement(By.linkText('¿Has olvidado tu correo electrónico?')).click()
    });
    it('Contactanos', async() => {
        await driver.findElement({ xpath: '/html/body/app-root/div/app-index/div/app-nav/nav/ul/li[5]/a/span' }).click()
        //await driver1.findElement({ xpath: '/html/body/app-root/div/app-index/div/app-nav/nav/ul/li[5]/a/span' }).click()
        //await driver1.findElement(By.id('mat-input-0')).sendKeys('yanitzabel')
        await driver.findElement(By.id('mat-input-0')).sendKeys('yanitzabel')
        await driver.findElement(By.id('mat-input-1')).sendKeys('04245370818')
        //await driver1.findElement(By.id('mat-input-1')).sendKeys('04245370818')
        await driver.findElement(By.id('mat-input-2')).sendKeys('para que sirve eso')
        //await driver1.findElement(By.id('mat-input-2')).sendKeys('para que sirve eso')
        await driver.findElement({ xpath: '//*[@id="contactanos"]/app-contactanos/div[2]/div/button' }).click()
        //await driver1.findElement({ xpath: '//*[@id="contactanos"]/app-contactanos/div[2]/div/button' }).click()

    });*/
    after(async() => {
        //driver.quit();
        driver1.quit()
    });
});